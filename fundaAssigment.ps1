﻿#https://docs.aws.amazon.com/powershell/latest/reference/items/Write-CWMetricAlarm.html
#https://stackoverflow.com/questions/55585013/aws-powershell-get-cwmetricstatistics-for-ebs-read-iops
#https://aws.amazon.com/blogs/developer/writing-and-archiving-custom-metrics-using-amazon-cloudwatch-and-aws-tools-for-powershell/

$ErrorActionPreference = "Stop"
$teamsFile = ".\teams_definition.json"
$alertsFile = ".\alerts_definition.json"
$accessKey = ""
$secretKey = ""
$region = "eu-west-1"
$monitoredMetricName = "ApproximateNumberOfMessagesVisible"
$monitoredNameSpace = "AWS/SQS"
$alertAlarmAction = "arn:aws:sns:eu-west-1:565610498406:Default_CloudWatch_Alarms_Topic"
$monitoringComparison = "GreaterThanThreshold"
$monitoringPeriod = "60"
$monitoringEvaulationPeriod = "1"
$monitoringDataPointsAlarm = "1"

function jsonConverter ($jsonInput)
{
$jsonInput = get-content $jsonInput
$objectOutput = $jsonInput | ConvertFrom-Json
return $objectOutput
}

function sqsDimensionCreator ($queue)
{
$dimension1 = New-Object Amazon.CloudWatch.Model.Dimension
$dimension1.set_Name("QueueName")
$dimension1.set_Value($queue)
return $dimension1
}

function teamQueueAssociationChecker ($queue, $teamIdToBeAlerted, $teams)
{
    if ((($teams.teams | where teamId -eq $teamIdToBeAlerted).queues.queueName) -contains $queue)
    {
        return 0
    }
    else
    {
        return 1
    }
}

function createAlert ($queue, $teamsIdToBeAlerted, $threshold,$teams)
{
    if ($alert.teamsIdToBeAlerted -eq "all")
    {
        foreach ($team in $teams.teams.teamId)
        {
            if ((teamQueueAssociationChecker -queue $queue -teamIdToBeAlerted $team -teams $teams) -eq 0)
            {
                Write-Host "Creating alert on the queue:" $queue "for the team:" $team "with the threshold:" $threshold
                $dimension = sqsDimensionCreator $queue
                $alarmName = $queue + "_threshold_" + $threshold + "_team_" + $team
                Write-CWMetricAlarm -AlarmName $alarmName -AlarmAction $alertAlarmAction -Namespace $monitoredNameSpace -Statistic "Maximum" -MetricName $monitoredMetricName -SecretKey $secretKey -AccessKey $accessKey -Region $region -Dimension $dimension -ComparisonOperator $monitoringComparison -Period $monitoringPeriod -EvaluationPeriod $monitoringEvaulationPeriod -DatapointsToAlarm $monitoringDataPointsAlarm -Threshold $threshold
            }
            else
            {
                Write-Host "The queue is not associated with the team"
            }
        }
    }
    else 
    {
        foreach ($teamIdToBeAlerted in $teamsIdToBeAlerted)
        {
            if ((teamQueueAssociationChecker -queue $queue -teamIdToBeAlerted $teamIdToBeAlerted -teams $teams) -eq 0)
            {
                Write-Host "Creating alert on the queue:" $queue "for the team:" $teamIdToBeAlerted "with the threshold:" $threshold
                $dimension = sqsDimensionCreator $queue
                $alarmName = $queue + "_threshold_" + $threshold + "_team_" + $teamIdToBeAlerted
                Write-CWMetricAlarm -AlarmName $alarmName -AlarmAction $alertAlarmAction -Namespace $monitoredNameSpace -Statistic "Maximum" -MetricName $monitoredMetricName -SecretKey $secretKey -AccessKey $accessKey -Region $region -Dimension $dimension -ComparisonOperator $monitoringComparison -Period $monitoringPeriod -EvaluationPeriod $monitoringEvaulationPeriod -DatapointsToAlarm $monitoringDataPointsAlarm -Threshold $threshold
            }
            else
            {
                Write-Host "The queue is not associated with the team"
            }
        }
    }
}

function processAlerts ($alerts,$teams)
{
    foreach ($alert in $alerts.alerts)
    {
    Write-Host "Processing alert with id:" $alert.alertid
    if ($alert.queueToMonitor -eq "all")
        {
        #Write-Host "all"
        foreach ($queue in $teams.teams.queues.queueName)
            {
                if ($alert.queueIncludedFilter -ne $null)
                {
                    $filter = $alert.queueIncludedFilter
                    $queuesToMonitor = $queue | Where-Object {$_ -like $filter}
                    foreach ($queueToMonitor in $queuesToMonitor)
                    {
                        createAlert -queue $queueToMonitor -threshold $alert.alertThreshold -teamsIdToBeAlerted $alert.teamsIdToBeAlerted.teamId -teams $teams

                    }
                }
                if ($alert.queueExcludedFilter -ne $null)
                {
                    $filter = $alert.queueExcludedFilter
                    $queuesToMonitor = $queue | Where-Object {$_ -notlike $filter}
                    foreach ($queueToMonitor in $queuesToMonitor)
                    {
                        createAlert -queue $queueToMonitor -threshold $alert.alertThreshold -teamsIdToBeAlerted $alert.teamsIdToBeAlerted.teamId -teams $teams
                    }
                }
            }
        }
    else
        {
        foreach ($queueToMonitor in $alert.queueToMonitor)
            {
            createAlert -queue $queueToMonitor.queueName -threshold $alert.alertThreshold -teamsIdToBeAlerted $alert.teamsIdToBeAlerted.teamId -teams $teams
            $queueList += $queueToMonitor.queueName
            $alertThresholdList += $alert.alertThreshold
            $teamIdList += $alert.teamsIdToBeAlerted.teamId
            }
        }
    }
}

$alerts = jsonConverter -jsonInput $alertsFile
$teams = jsonConverter -jsonInput $teamsFile
processAlerts -alerts $alerts -teams $teams